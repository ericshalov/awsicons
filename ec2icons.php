<?php
//
//	AWSicons
//	ec2icons.php
//	Make icons for EC2 instance sizes.
//
//	(C) Copyright 2015 Eric Shalov.
//	Amazon, AWS, and EC2 are trademarks of Amazon.com, Inc. or its affiliates.
//
//	Required: libgd


$ec2_instances = array(
  // Announced Jun 11, 2015:
  "m4.large"    => array("ram"  =>     "8",  "vcpu" =>  "2"),
  "m4.xlarge"   => array("ram"  =>    "16",  "vcpu" =>  "4"),
  "m4.2xlarge"  => array("ram"  =>    "32",  "vcpu" =>  "8"),
  "m4.4xlarge"  => array("ram"  =>    "64",  "vcpu" => "16"),
  "m4.10xlarge" => array("ram"  =>   "160",  "vcpu" => "40"),

  // Current Generation (as of Jun 2015):
  "t2.micro"    => array("ram"  =>     "1",  "vcpu" =>  "1"),
  "t2.small"    => array("ram"  =>     "2",  "vcpu" =>  "1"),
  "t2.medium"   => array("ram"  =>     "4",  "vcpu" =>  "2"),
  "m3.medium"   => array("ram"  =>  "3.75",  "vcpu" =>  "1"),
  "m3.large"    => array("ram"  =>   "7.5",  "vcpu" =>  "2"),
  "m3.xlarge"   => array("ram"  =>    "15",  "vcpu" =>  "4"),
  "m3.2xlarge"  => array("ram"  =>    "30",  "vcpu" =>  "8"),
  "c4.large"    => array("ram"  =>  "3.75",  "vcpu" =>  "2"),
  "c4.xlarge"   => array("ram"  =>   "7.5",  "vcpu" =>  "4"),
  "c4.2xlarge"  => array("ram"  =>    "15",  "vcpu" =>  "8"),
  "c4.4xlarge"  => array("ram"  =>    "30",  "vcpu" => "16"),
  "c4.8xlarge"  => array("ram"  =>    "60",  "vcpu" => "36"),
  "c3.large"    => array("ram"  =>  "3.75",  "vcpu" =>  "2"),
  "c3.xlarge"   => array("ram"  =>   "7.5",  "vcpu" =>  "4"),
  "c3.2xlarge"  => array("ram"  =>    "15",  "vcpu" =>  "8"),
  "c3.4xlarge"  => array("ram"  =>    "30",  "vcpu" => "16"),
  "c3.8xlarge"  => array("ram"  =>    "60",  "vcpu" => "32"),
  "g2.2xlarge"  => array("ram"  =>    "15",  "vcpu" =>  "8"),
  "g2.8xlarge"  => array("ram"  =>    "60",  "vcpu" => "32"),
  "r3.large"    => array("ram"  => "15.25",  "vcpu" =>  "2"),
  "r3.xlarge"   => array("ram"  =>  "30.5",  "vcpu" =>  "4"),
  "r3.2xlarge"  => array("ram"  =>    "61",  "vcpu" =>  "8"),
  "r3.4xlarge"  => array("ram"  =>   "122",  "vcpu" => "16"),
  "r3.8xlarge"  => array("ram"  =>   "244",  "vcpu" => "32"),
  "i2.xlarge"   => array("ram"  =>  "30.5",  "vcpu" =>  "4"),
  "i2.2xlarge"  => array("ram"  =>    "61",  "vcpu" =>  "8"),
  "i2.4xlarge"  => array("ram"  =>   "122",  "vcpu" => "16"),
  "i2.8xlarge"  => array("ram"  =>   "244",  "vcpu" => "32"),
  "d2.xlarge"   => array("ram"  =>  "30.5",  "vcpu" =>  "4"),
  "d2.2xlarge"  => array("ram"  =>    "61",  "vcpu" =>  "8"),
  "d2.4xlarge"  => array("ram"  =>   "122",  "vcpu" => "16"),
  "d2.8xlarge"  => array("ram"  =>   "244",  "vcpu" => "36"),

  // Previous Generation
  "m1.small"    => array("ram"  =>   "1.7",  "vcpu" =>  "1"),
  "m1.medium"   => array("ram"  =>  "3.75",  "vcpu" =>  "1"),
  "m1.large"    => array("ram"  =>   "7.5",  "vcpu" =>  "2"),
  "m1.xlarge"   => array("ram"  =>    "15",  "vcpu" =>  "4"),
  "c1.medium"   => array("ram"  =>   "1.7",  "vcpu" =>  "2"),
  "c1.xlarge"   => array("ram"  =>     "7",  "vcpu" =>  "8"),
  "cc2.8xlarge" => array("ram"  =>  "60.5",  "vcpu" => "32"),
  "m2.xlarge"   => array("ram"  =>  "17.1",  "vcpu" =>  "2"),
  "m2.2xlarge"  => array("ram"  =>  "34.2",  "vcpu" =>  "4"),
  "m2.4xlarge"  => array("ram"  =>  "68.4",  "vcpu" =>  "8"),
  "cr1.8xlarge" => array("ram"  =>   "244",  "vcpu" => "32"),
  "hi1.4xlarge" => array("ram"  =>  "60.5",  "vcpu" => "16"),
  "hs1.8xlarge" => array("ram"  =>   "117",  "vcpu" => "16"),
  "t1.micro"    => array("ram"  => "0.615",  "vcpu" =>  "1")
);


function box($im, $x1,$y1, $x2,$y2, $color) {
   imagepolygon ($im , array(
     $x1,$y1,
     $x2,$y1,
     $x2,$y2,
     $x1,$y2,
     $x1,$y1
   ), 5, $color);
}

function filledbox($im, $x1,$y1, $x2,$y2, $color) {
   imagefilledpolygon ($im , array(
     $x1,$y1,
     $x2,$y1,
     $x2,$y2,
     $x1,$y2,
     $x1,$y1
   ), 5, $color);
}

function rounded_box($im, $x1,$y1, $x2,$y2, $rounding, $color) {
  // (0 degrees is east)
  // upper-left
  imagefilledarc($im, $x1+$rounding,$y1+$rounding,
                 $rounding*2,$rounding*2,
                 180,270, $color, IMG_ARC_PIE);
                 
  // upper-right            
  imagefilledarc($im, $x2-$rounding,$y1+$rounding,
                 $rounding*2,$rounding*2,
                 270,360, $color, IMG_ARC_PIE);

  // lower-left
  imagefilledarc($im, $x1+$rounding,$y2-$rounding,
                 $rounding*2,$rounding*2,
                 90,180, $color, IMG_ARC_PIE);
                 
  // lower-right            
  imagefilledarc($im, $x2-$rounding,$y2-$rounding,
                 $rounding*2,$rounding*2,
                 0,90, $color, IMG_ARC_PIE);

  imagefilledrectangle($im, $x1+$rounding,$y1, $x2-$rounding,$y2, $color);
  imagefilledrectangle($im, $x1,$y1+$rounding, $x1+$rounding,$y2-$rounding, $color);
  imagefilledrectangle($im, $x2-$rounding,$y1+$rounding, $x2,$y2-$rounding, $color);
}



// draw VCPUs:
function draw_vcpus($im, $instance_size, $color) {
  global $ec2_instances;
  global $colors;
  
}

$num_instances = 0;

foreach($ec2_instances as $instance_size => $instance_details) {
      $vcpu_columns = floor(($instance_details["vcpu"]-1) / 4) + 1;
      $ram_columns  = floor((ceil($instance_details["ram"])-1) / 8) + 1;

      $width = 60 + (($vcpu_columns) * 5) + $ram_columns*4+5;
      $height = 40;

      $im = imagecreate($width, $height);

      $colors = array();
      $colors["white"] = imagecolorallocate($im, 255,255,255);
      $colors["orange"] = imagecolorallocate($im, 250, 160, 60);
      $colors["darkorange"] = imagecolorallocate($im, 125, 80, 35);
      $colors["black"] = imagecolorallocate($im, 0, 0, 0);
      $colors["gray"] = imagecolorallocate($im, 127,127,127);
      $colors["darkblue"] = imagecolorallocate($im, 20,20,80);


      imagefilledrectangle($im, 0,0, $width-1,$height-1, $colors["white"]);
      rounded_box($im, 0,0+2, 60 + (($vcpu_columns) * 5) + $ram_columns*4+5,
                              37+2, 5, $colors["darkorange"]); // shadow

      rounded_box($im, 0,0,   60 + (($vcpu_columns) * 5) + $ram_columns*4+5,
                              37, 5, $colors["orange"]);


      // Draw vCPU's:
      $xoff = 60;

      filledbox($im, $xoff,0,   $xoff + (($vcpu_columns) * 5),
                              37, $colors["gray"]);
      $col=0;
      $row=0;
      for($count=0;$count<$instance_details["vcpu"];$count++) {
        filledbox($im, $xoff+$col*5+1,  6+$row*7,
                 $xoff+$col*5+4,  6+$row*7+5, $colors["white"]);
        ++$row;
        if($row==4) { $row = 0; ++$col; }
      }

      $xoff += $vcpu_columns*5 + 3;

      // Draw RAM - 1 block per GB
      $col=0;
      $row=0;
      for($count=0;$count<ceil($instance_details["ram"]);$count++) {
        box($im, $xoff+$col*4,  4+$row*4,
                 $xoff+$col*4+2,4+$row*4+2, $colors["darkblue"]);
        ++$row;
        if($row==8) { $row = 0; ++$col; }
      }

      imagettftext($im, 8, 0,
        5,10,
        $colors["black"],
        "./fonts/DroidSerif-Regular.ttf",
        $instance_size );
      imagettftext($im, 8, 0,
        5,22,
        $colors["black"],
        "./fonts/DroidSerif-Regular.ttf",
        $instance_details["vcpu"] . " vCPU");
      imagettftext($im, 8, 0,
        5,34,
        $colors["black"],
        "./fonts/DroidSerif-Regular.ttf",
        $instance_details["ram"]." GB");


      //-------------------------- Output the image ------------------------------
      imagepng($im,  "imgs/".$instance_size.".png");
      imagejpeg($im, "imgs/".$instance_size.".jpg");
      imagegif($im,  "imgs/".$instance_size.".gif");
      imagegd2($im,  "imgs/".$instance_size.".gd2");
      ++$num_instances;

      imagedestroy($im);
}
echo "Icons generated for $num_instances instances.\n";

?>
