awsicons
=========

AWSicons is a PHP script that generates a collection of icons for use with
monitoring applications such as Nagios.

![imgs/c3.2xlarge.jpg](imgs/c3.2xlarge.jpg "Icon for c3.2xlarge EC2 instance.")
![imgs/d2.2xlarge.jpg](imgs/d2.2xlarge.jpg "Icon for d2.2xlarge EC2 instance.")

Downloads:
----------
Clone the repo:

$ git clone 'https://github.com/EricShalov/awsicons.git'

License:
--------
AWSicons is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name Eric Shalov, the name AWSicons nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
